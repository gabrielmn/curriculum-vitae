% Class file header
% Specifies the LaTeX version required
\NeedsTeXFormat{LaTeX2e}
% Identifies this as a class file for a curriculum vitae, with the creation date
\ProvidesClass{curriculum-vitae}[2023/04/28 Curriculum Vitae Class]
% Default language setting
% Initializes the language setting to English (US)
\newcommand{\@language}{en-us}
% Language options
% Option for Brazilian Portuguese
\DeclareOption{pt-br}{
    % Changes the language setting to Portuguese (Brazil)
    \renewcommand{\@language}{pt-br}
}
% Option for English (US)
\DeclareOption{en-us}{
    % Reinforces the language setting to English (US)
    \renewcommand{\@language}{en-us}
}
% Processes the provided options and relaxes afterward
\ProcessOptions\relax  
% Class loading
% Loads the base article class with a default font size of 12pt
\LoadClass[12pt]{article} 
% Package Importation
% This section imports all necessary packages for the layout, images, icons, fonts, and additional features.
% Conditional statements support
\RequirePackage{ifthen}
% Layout Configuration
% The geometry package is used to define the physical layout of the pages including margins, text block size, and footer.
\RequirePackage[
    a4paper,                % Set paper size to A4
    hoffset=0pt,            % Horizontal offset
    voffset=0pt,            % Vertical offset
    headheight=0pt,         % Header height
    headsep=0pt,            % Separation between header and text
    footskip=32pt,          % Distance from bottom of text to footer
    marginparwidth=0pt,     % Width of marginal notes
    marginparsep=0pt,       % Separation between marginal notes and text
    left=16pt,              % Left margin
    right=16pt,             % Right margin
    top=16pt,               % Top margin
    bottom=16pt,            % Bottom margin
    includefoot             % Include footer in the layout calculation
]{geometry}
% Image Handling
% Support for graphics
\RequirePackage{graphicx}
% Drawing package
\RequirePackage{tikz}
% Icons
% Icon font package
\RequirePackage{fontawesome}
% Fonts Configuration
% Sans-serif font for the document
\RequirePackage[default]{sourcesanspro}
% Font encoding
\RequirePackage[T1]{fontenc}
% Colors
\RequirePackage{xcolor}
% Page Numbering
% Reference the last page in the document
\RequirePackage{lastpage}
% Custom header and footer  
\RequirePackage{fancyhdr}
% Hyperlinks
% Hyperlinking capabilities
\RequirePackage{hyperref}
% Settings for Document Appearance
%% Page Numbering Style
% The fancyhdr package is used to define custom headers and footers.
\pagestyle{fancy}
% Clear all header fields
\fancyhead{}
% Clear all footer fields
\fancyfoot{}
% Remove the header rule
\renewcommand{\headrulewidth}{0pt}
% Footer Configuration
% "Page X of Y" will be displayed in the right-hand side of the footer.
\fancyfoot[R]{
    \textcolor{primary}{\@addTitle{Page}{Página} \thepage\ \@addTitle{of}{de}\ \pageref{LastPage}}
}
% Hyperlink Settings
% Configures the appearance of hyperlinks in the document.
\hypersetup{
    hidelinks   % Hide link borders
}
% Paragraph Settings
% Set paragraph indentation to zero to avoid text offset at the beginning of paragraphs.
\setlength{\parindent}{0pt}
% Command Definitions

\definecolor{primary}{RGB}{0,0,0}
\definecolor{secondary}{RGB}{75,75,75}
\color{secondary}

\newcommand{\setprimarycolor}[1]{
    \definecolor{primary}{RGB}{#1}
    }
\newcommand{\setsecondarycolor}[1]{
    \definecolor{secondary}{RGB}{#1}
    \color{secondary}
}

% Header Command
% Creates the header of the curriculum vitae, including a photo, personal details, and contact information.
\newcommand{\header}[9]{
    \begin{flushleft}
        \makebox[549pt]{
            \textcolor{primary}{
                \parbox[][96pt][]{400pt}{
                    \vspace{3pt}
                    \textbf{\fontsize{50}{60}\selectfont#2}
                    \vfill
                    \textbf{\fontsize{50}{60}\selectfont#3}
                    \vspace{3pt}
                }            
            }    
            \hfill
            \parbox[][96pt][]{96pt}{
                \begin{tikzpicture}
                    \node [inner sep=0pt, clip, rounded corners=12pt] at (0,0) {\includegraphics[width=96pt]{#1}};
                \end{tikzpicture}  
            }
            \hspace{16pt}
        }
    \end{flushleft}
    
    \begin{flushleft}
        
        \makebox[549pt]{
            \parbox{170pt}{
                \faWhatsapp \space \href{tel:#4}{#4}
                \\
                \faEnvelope \space \href{mailto:#5}{#5}
            }
            \hfill    
            \parbox{170pt}{
                \faGlobe \space \href{https://#9}{#9}
                \\
                \faLinkedin \space \href{#6}{#2}
            }
            \hfill
            \parbox{170pt}{
                \faGitlab \space \href{https://gitlab.com/#7}{#7}
                \\
                \faGithub \space \href{https://github.com/#8}{#8}
            }
            \hspace{16pt}
        }
    \end{flushleft}
}
% Section Command
% Defines the appearance of section headers in the CV.
\renewcommand{\section}[1]{
   
    \noindent\textcolor{primary}{\textbf{\textit{#1}}}
    \par
    \vspace{-10pt}
    \noindent\textcolor{primary}{\rule{\textwidth}{0.1pt}}
    
}
% List With Commas Command
% Used for listing items in a sequence with commas.
\newif\if@next
\newcommand{\@listWithCommas}[1]{
    % Check if #1 is \relax    
    \ifthenelse{\equal{#1}{\relax}}%
    {.}%
    {%
        % Check if \@next is true
        \ifthenelse{\boolean{@next}}%
        {%
            ,\space#1%
            \expandafter\@listWithCommas%
        }%
        {%
            \@nexttrue%
            #1\expandafter\@listWithCommas%
        }%
    }%
}
% Title Addition Command
% Adds a title based on the set language.
\newcommand{\@addTitle}[2]{
    \ifthenelse{\equal{\@language}{en-us}}
    {#1}  % English title
    {
        \ifthenelse{\equal{\@language}{pt-br}}
        {#2}  % Portuguese title
        {language not supported}  % Fallback text
    }
}
% Goals Section Commands
% Defines the structure for the Goals section of the CV.
% Command to introduce the Goals section with a title
\newcommand{\goals}{
    \textcolor{primary}{    
        \section{
            \@addTitle{Goals}{Objetivos}
        }
    }
}
% Command to add the actual goals content
\newcommand{\addGoals}[1]{
    \begin{flushright}        
        \textcolor{secondary}{
            \makebox[549pt]{
                \parbox{533pt}{
                    \textit{#1}
                }
                \hspace{16pt}
            }
        }
    \end{flushright} 
}
% Education Section Commands
% Defines the structure for the Education section of the CV.
% Command to introduce the Education section with a title
\newcommand{\education}{
    \textcolor{primary}{    
        \section{
            \@addTitle{Education}{Educação}
        }
    }
}
% Command to add individual education entries
\newcommand{\addEducation}[3]{
    \begin{flushright}
        % Institution name and dates
        \textcolor{secondary}{
            \makebox[549pt]{
                \textbf{#1}
                \hfill
                \textit{#2}
                \hspace{16pt}
            }
        }
        % Degree or qualification
        \textcolor{secondary}{
            \makebox[533pt]{
                \textit{#3}
                \hfill
                \hspace{16pt}
            }
        }
    \end{flushright}
}
% Experience Section Commands
% Defines the structure for the Experience section of the CV.
% Command to introduce the Experience section with a title
\newcommand{\experience}{
    \textcolor{primary}{    
        \section{
            \@addTitle{Experience}{Experiência}
        }
    }
}
% Command to add individual experience entries
\newcommand{\addExperience}[6]{
    \begin{flushright}
        % Job title and dates
        \textcolor{secondary}{
            \makebox[549pt]{
                \textbf{#1}
                \hfill
                \textit{#2 - #3}
                \hspace{16pt}
            }
        }
        % Company and location
        \textcolor{secondary}{
            \makebox[533pt]{
                \textit{#4}
                \hfill
                \textit{#5}
                \hspace{16pt}
            }
        }
        % Job description
        \@addExperienceDescription#6\relax  
    \end{flushright}
}
% Internal command to format the experience description
\newcommand{\@addExperienceDescription}[1]{
    \ifthenelse{\equal{#1}{\relax}}
    {}
    {
        % Description with a bullet point
        \textcolor{secondary}{
            \makebox[517pt]{
                - #1
                \hfill
                \hspace{16pt}
            }
        }
        \par
    	\expandafter\@addExperienceDescription
  	}
}
% Personal Projects Section Commands
% Defines the structure for the Personal Projects section of the CV.
\newcommand{\personalProjects}{
    \textcolor{primary}{        
        \section{
            \@addTitle{Personal Projects}{Projetos Pessoais}
        }
    }
}
\newcommand{\addPersonalProject}[5]{
    \begin{flushright}
          % Project name and date range
        \textcolor{secondary}{
            \makebox[549pt]{
                \textbf{#1}
                \hfill
                \textit{#2 - #3}
                \hspace{16pt}
            }
        }
        \par
        \vspace{8pt}
        % Project description
        \textcolor{secondary}{
            \makebox[533pt]{
                \parbox{405pt}{
                    \textit{#4}
                }
                \hspace{128pt}
            }
        }
        \par
        \vspace{8pt}
        \ifthenelse{\equal{#5}{}}
        {
            % No tools specified
            \relax
        }
        {
            \@projectDetails#5\relax
        }
    \end{flushright}
}
\newcommand{\@projectDetails}[1]{
    \ifthenelse{\equal{#1}{\relax}}
    {}
    {
        % Description with a bullet point
        \@projectDetailsLine#1
        \par
    	\expandafter\@projectDetails
  	}
}
\newcommand{\@projectDetailsLine}[2]{
    \ifthenelse{\equal{#1}{\relax}}
    {}
    {
        \@nextfalse
        \textcolor{secondary}{
            \makebox[517pt]{
                \parbox{485pt}{
                    \textbf{- #1:}
                    \@listWithCommas#2
                    \relax                    
                }
                \hfil
                \hspace{32pt}
            }
        }
  	}
}
% Skills Section Commands
% Defines the structure for listing various skills in the CV.
\newcommand{\skills}{
    \textcolor{primary}{    
        \section{
            \@addTitle{Hard and Soft Skills}{Competencias e Habilidades}
        }
    }
}
\newcommand{\addSkillSection}[2]{
    \@nextfalse
    \begin{flushright}
        \textcolor{secondary}{
            \makebox[549pt]{
            \parbox{533pt}{
                \textbf{#1:}
                \textit{\@listWithCommas#2\relax}
            }
            \hspace{16pt}
            }
        }
    \end{flushright}
}
% The following commands use the generic \addSkillSection to add specific skill types.
\newcommand{\addSoftSkills}[1]{
    \addSkillSection{\@addTitle{Soft Skills}{Competencias}}{#1}
}
\newcommand{\addProgrammingLanguages}[1]{
    \addSkillSection{\@addTitle{Programming Languages}{Linguagens de Programação}}{#1}
}
\newcommand{\addMarkupLanguages}[1]{
    \addSkillSection{\@addTitle{Markup Languages}{Linguagens de Marcação}}{#1}
}
\newcommand{\addQueryLanguage}[1]{
    \addSkillSection{\@addTitle{Query Language}{Linguagem de Consulta}}{#1}
}
\newcommand{\addFrameworksLibraries}[1]{
    \addSkillSection{\@addTitle{Frameworks \& Libraries}{Frameworks e Bibliotecas}}{#1}
}
\newcommand{\addTestingFrameworksLibraries}[1]{
    \addSkillSection{\@addTitle{Testing Frameworks \& Libraries}{Frameworks e Bibliotecas de Teste}}{#1}
}
\newcommand{\addCloud}[1]{
    \addSkillSection{\@addTitle{Cloud}{Nuvem}}{#1}
}
\newcommand{\addDatabase}[1]{
    \addSkillSection{\@addTitle{Database}{Banco de Dados}}{#1}
}
\newcommand{\addOffice}[1]{
    \addSkillSection{\@addTitle{Office}{Office}}{#1}
}
\newcommand{\addWindowsServer}[1]{
    \addSkillSection{\@addTitle{Windows Server}{Servidor Windows}}{#1}
}
% Certificates Section Commands
% Defines the structure for the Certificates section of the CV.
\newcommand{\certificates}{
    \textcolor{primary}{    
        \section{
            \@addTitle{Certificates}{Certificados}
        }
    }
}

\newcommand{\addCertificate}[3]{
    \begin{flushright}
        % Certificate name and issuing organization
        \textcolor{secondary}{
            \makebox[549pt]{
                \textbf{#1}
                \hfill
                \textit{#2}
                \hspace{16pt}
            }
        }
        % Additional details or date
        \textcolor{secondary}{
            \makebox[533pt]{
                \textit{#3}
                \hfill
                \hspace{16pt}
            }
        }
    \end{flushright}    
}