#!/bin/sh

for tex_file in gabrielmn_curriculum_vitae.*.tex; do
  if [ -f "$tex_file" ]; then
    pdflatex "$tex_file"
    if ! pdflatex "$tex_file";then
      exit 1
    fi
  else
    echo "No .tex files found matching pattern."
  fi
done